import openpyxl
import pandas
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

file = "SpringerEbooks.xlsx"
data = pandas.ExcelFile(file)
sheet_name = ''.join(data.sheet_names)
sheet = openpyxl.load_workbook(file)[sheet_name]
driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)

for row in range(2, sheet.max_row + 1):
    url = sheet['E' + str(row)].value
    driver.get(url)
    driver.maximize_window()
    # inside the condition, By statement and locator have to be a tuple
    # variable added for readability
    download_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "cta-button-container__item")))
    download_button.click()

driver.close()
